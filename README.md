# okular

## Ubuntu FreeHand

![](https://gitlab.com/openbsd98324/okular/-/raw/main/medias/Screenshot_from_2024-07-01_11-05-26.png)

```
 ii  libokular5core9                               4:21.12.3-0ubuntu1                      amd64        libraries for the Okular document viewer
ii  okular                                        4:21.12.3-0ubuntu1                      amd64        universal document viewer
ii  okular-extra-backends                         4:21.12.3-0ubuntu1                      amd64        additional document format support for Okular
```


## Edit a PDF

manjaro x86 64, Linux manjaro 4.9.0-11-amd64 #1 SMP Debian 4.9.189-3 (2019-09-02) x86_64 GNU/Linux


![](medias/okular.png)

